﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
   [SerializeField] private TextMeshProUGUI scoreText;
   [SerializeField] private TextMeshProUGUI PlayerScoreText;

   private GameManager gameManager;
   private int playerScore;
   public void Init(GameManager gameManager)
   {
      this.gameManager = gameManager;  
      this.gameManager.OnRestarted += OnRestarted;
      HideScore(false);
      ScorePlus(0);
   }

   public void SetScore(int score)
   {
      playerScore = score;
      scoreText.text = $"Score : {playerScore}";
   }

   public void ScorePlus(int score)
   {
      playerScore += score;
      scoreText.text = $"Score : {playerScore}";
   }

   private void Awake()
   {
      Debug.Assert(scoreText != null, "scoreText cannot be null");
   }

   private void OnRestarted()
   {
      PlayerScoreText.text = $"Your Score : {playerScore}";
      gameManager.OnRestarted -= OnRestarted;
      HideScore(true);
   }

   public void ScoreScreen(bool yes)
   {
      PlayerScoreText.gameObject.SetActive(yes);
   }
   public void HideScore(bool hide)
   {
      scoreText.gameObject.SetActive(!hide);
      
   }
}
