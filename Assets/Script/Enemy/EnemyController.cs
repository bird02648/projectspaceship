﻿using Script.SpaceShip;
using UnityEngine;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        
        [SerializeField] private EnemySpaceShip enemySpaceShip;
        [SerializeField] private float chasingThresholdDistance;
        [SerializeField] private Rigidbody2D rigidbody2D;
        private float moveTime = 2 ;
        private float moveleft = 4.5f;

        private void Update()
        {
            moveTime += Time.deltaTime;
            Move();
            enemySpaceShip.Fire();
        }

        private void Move()
        {
            
            if (moveTime < moveleft)
            {
                MoveSet(Vector2.left);
            }
            if (moveTime > moveleft)
            {
                MoveSet(Vector2.right);
            }
            if (moveTime >= 9)
            {
                moveTime = 0;
            }
        }

        private void MoveSet(Vector2 direction)
        {
            rigidbody2D.velocity = direction * enemySpaceShip.Speed;
        }


        
    }    
}