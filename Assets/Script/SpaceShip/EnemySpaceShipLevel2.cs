﻿using System;
using UnityEngine;

namespace Script.SpaceShip
{
    public class EnemySpaceShipLevel2 : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private Bullet dualBullet;
        [SerializeField] private Bullet untimateBullet;
        [SerializeField] private Transform dualGunPosision;
        [SerializeField] private Transform untimateGunPosision;
        [SerializeField] private double fireRate = 1;
        [SerializeField] private double untimateFireRate = 5;
        [SerializeField] private AudioClip enemyLevel2FireSounds;
        [SerializeField] private float enemyLevel2FireSoundsValume = 0.1f;
        [SerializeField] private AudioClip utimateFireSounds;
        [SerializeField] private float utimateFireSoundsValume = 0.3f;
        [SerializeField] private AudioClip enemyLevel2ExplodedSounds;
        [SerializeField] private float enemyLevel2ExplodedSoundsValume = 0.3f;
        
        private float fireCounter = 0;
        private float UntimatefireCounter = 0;
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void TakeHeal(int heal)
        {
            
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyLevel2ExplodedSounds,Camera.main.transform.position,enemyLevel2ExplodedSoundsValume);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            // TODO : Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter > fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyLevel2FireSounds,Camera.main.transform.position,enemyLevel2FireSoundsValume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                var dualbullet = Instantiate(dualBullet, dualGunPosision.position, Quaternion.identity);
                dualbullet.Init(Vector2.down);
                fireCounter = 0;

            }
            UntimatefireCounter += Time.deltaTime;
            if (UntimatefireCounter>untimateFireRate)
            {
                AudioSource.PlayClipAtPoint(utimateFireSounds,Camera.main.transform.position,utimateFireSoundsValume);
                var untimateBullet =
                    Instantiate(this.untimateBullet, untimateGunPosision.position, Quaternion.identity);
                untimateBullet.Init(Vector2.down);
                UntimatefireCounter = 0;
            }
        }
    }
}