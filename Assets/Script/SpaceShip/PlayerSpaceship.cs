﻿using System;
using UnityEngine;
using UnityEngine.UI;   
namespace Script.SpaceShip
{
    public class PlayerSpaceship : BaseSpaceShip,IDamagable,IHealable
    {
        [SerializeField] private PlayerHpBar hpBar;
        [SerializeField] private AudioClip playerFireSounds;
        [SerializeField] private float playerFireSoundsValume = 0.1f;
        [SerializeField] private AudioClip playerExplodedSounds;
        [SerializeField] private float playerExplodedSoundsValume = 0.3f;
        public event Action OnExploded;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null,"defaultBullet cannot be null"  );
            Debug.Assert(gunPosition != null,"gunPosition cannot be null"  );
        }
        public void SetPlayer(int hp, float speed)
        {
           // base.Init(hp,speed,defaultBullet);
           Speed = speed;
           Hp = hp;
           hpBar.SetMaxHealth(hp);
           hpBar.SetHealth(hp);
        }

        
        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSounds,Camera.main.transform.position,playerFireSoundsValume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }
        
        public void TakeHit(int damage)
        {
            
             Hp -= damage;
             hpBar.SetHealth(Hp);
             if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void TakeHeal(int heal)
        {

            Hp += heal;
            if (Hp > hpBar.slider.maxValue)
            {
                Hp = (int) hpBar.slider.maxValue;
            }
            
            hpBar.SetHealth(Hp);
            
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerExplodedSounds,Camera.main.transform.position,playerExplodedSoundsValume);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            //Destroy(gameObject);
            gameObject.SetActive(false);
            OnExploded?.Invoke();
        }
    }
}