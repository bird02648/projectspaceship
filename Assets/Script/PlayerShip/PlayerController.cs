﻿using Script.SpaceShip;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceShip;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;


        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceShip.Fire();
        } 

        private void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceShip.Speed;

            var newPosition = transform.position;
            newPosition.x = transform.position.x + inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inputVelocity.y * Time.smoothDeltaTime;

            newPosition.x = Mathf.Clamp(newPosition.x, xMin, xMax);
            newPosition.y = Mathf.Clamp(newPosition.y, yMin, yMax);
            transform.position = newPosition;
        }

        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null,"Main camera cannot be null");

            var spriteRenderer = playerSpaceShip.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");

            var offset = spriteRenderer.bounds.size;
            xMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            xMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            yMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            yMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }      
}
