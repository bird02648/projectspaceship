﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class HealerManager : MonoBehaviour
{
       [SerializeField] private Healer healer1;
       [SerializeField] private Transform healerPosision1;
       [SerializeField] private Healer healer2;
       [SerializeField] private Transform healerPosision2;
       [SerializeField] private Healer healer3;
       [SerializeField] private Transform healerPosision3;
       [SerializeField] private Healer healer4;
       [SerializeField] private Transform healerPosision4;
       static Random random = new Random();
       private float shootcount = 0;
       private float healerShoot = 5;
       private void Update()
       {
          Fire();
       }
    
       public  void Fire()
       {
          shootcount += Time.deltaTime;
          if (shootcount > healerShoot)
          {
             var numberRandom = random.Next(1, 5);
             if (numberRandom ==1)
             {
                var healer = Instantiate(healer1, healerPosision1.position, Quaternion.identity);
                healer.Init(Vector2.down);
             }
    
             if (numberRandom ==2)
             {
                var healer = Instantiate(healer2, healerPosision2.position, Quaternion.identity);
                healer.Init(Vector2.down);
             }
    
             if (numberRandom == 3)
             {
                var healer = Instantiate(healer3, healerPosision3.position, Quaternion.identity);
                healer.Init(Vector2.down);
             }
    
             if (numberRandom ==4)
             {
                var healer = Instantiate(healer4, healerPosision4.position, Quaternion.identity);
                healer.Init(Vector2.down);
             }
    
             shootcount = 0;
          }
       }

}
