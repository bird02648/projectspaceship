﻿using System;
using System.Collections;
using System.Collections.Generic;
using Script.SpaceShip;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Button exitButton;
    [SerializeField] private Button nextlevelButton;
    [SerializeField] private Button backToMenuButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button startButton;
    [SerializeField] private PlayerSpaceship playerSpaceShip;
    [SerializeField] private PlayerHpBar hpBar;
    [SerializeField] private EnemySpaceShip enemySpaceShip;
    [SerializeField] private EnemySpaceShipLevel2 enemySpaceShipLevel2;
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private RectTransform dialog;
    [SerializeField] private RectTransform uiRestart;
    [SerializeField] private RectTransform nextLevel;
    [SerializeField] private HealerManager healer;
    [SerializeField] private ShootingStartScript shootingstar;
    public event Action OnRestarted;
    [SerializeField] private int playerSpaceShipHp;
    [SerializeField] private int playerSpaceShipSpeed;
    [SerializeField] private int enemySpaceShipHp;
    [SerializeField] private int enemySpaceShipMoveSpeed;
    
    private void Awake()
    {
        Debug.Assert(backToMenuButton != null,"backToMenuButton cannot be null" );
        Debug.Assert(nextLevel != null,"nextLevel cannot be null" );
        Debug.Assert(restartButton != null,"restartButton cannot be null" );
        Debug.Assert(startButton != null, "startButton cannot be null");
        Debug.Assert(dialog != null, "dialog cannot be null");
        Debug.Assert(playerSpaceShip != null, "playerSpaceShip cannot be null");
        Debug.Assert(enemySpaceShip != null, "enemySpaceShip cannot be null");
        Debug.Assert(scoreManager != null, "scoreManager cannot be null");
        Debug.Assert(enemySpaceShipHp > 0, "enemySpaceShipHp has to be more than zero");
        Debug.Assert(enemySpaceShipMoveSpeed > 0, "enemySpaceShipMoveSpeed has to be more than zero");
        restartButton.onClick.AddListener(OnRestartButtonOnclicked);
        startButton.onClick.AddListener(OnStartButtonClicked);
        nextlevelButton.onClick.AddListener(OnNextLevelButtonOnclicked);
        backToMenuButton.onClick.AddListener(OnBackToMenuButtonOnClicked);
        exitButton.onClick.AddListener(OnBackToMenuButtonOnClicked);
        
    }

    private void Update()
    {
        
    }
    private void OnStartButtonClicked()
    {
        
        dialog.gameObject.SetActive(false);
        StartGame();
    }

    private void StartGame()
    {
        scoreManager.Init(this);
        SpawnPlayerSpaceship();
        SpawnEnemySpaceship();
    }

    private void SpawnPlayerSpaceship()
    {
        /* var spaceship = Instantiate(playerSpaceShip);
        spaceship.Init(playerSpaceShipHp, playerSpaceShipMoveSpeed);
        spaceship.OnExploded += OnPlayerSpaceshipExploded; */
        SpawnHpBarAndPlayer();
        playerSpaceShip.SetPlayer(playerSpaceShipHp,playerSpaceShipSpeed);
        playerSpaceShip.OnExploded += OnPlayerSpaceshipExploded;
    }

    private void SpawnHpBarAndPlayer()
    {
        hpBar.gameObject.SetActive(true);
        playerSpaceShip.gameObject.SetActive(true);
        healer.gameObject.SetActive(true);
        shootingstar.gameObject.SetActive(true);
    }
    private void DeSpawnHpBarAndPlayer()
    {
        hpBar.gameObject.SetActive(false);
        playerSpaceShip.gameObject.SetActive(false);
        healer.gameObject.SetActive(false);
        shootingstar.gameObject.SetActive(false);
    }
    private void OnPlayerSpaceshipExploded()
    {
        Restart();
    }

    private void SpawnEnemySpaceship()
    {
        var spaceship = Instantiate(enemySpaceShip);
        spaceship.Init(enemySpaceShipHp, enemySpaceShipMoveSpeed);
        spaceship.OnExploded += OnEnemySpaceshipExploded;
    }

    private void SpawnEnemySpaceshipLevel2()
    {
        var spaceship = Instantiate(enemySpaceShipLevel2);
        spaceship.Init(enemySpaceShipHp*2, enemySpaceShipMoveSpeed*1.5f);
        spaceship.OnExploded += OnEnemySpaceshipLV2Exploded;
    }

    private void OnEnemySpaceshipExploded()
    {
        DestroyRemainingShips();
        scoreManager.ScorePlus(+1);
        nextLevel.gameObject.SetActive(true);
    }
    
    private void OnEnemySpaceshipLV2Exploded()
    {
        scoreManager.ScorePlus(+1);
        SpawnEnemySpaceshipLevel2();
    }

    private void Restart()
    {
        
        DestroyRemainingShips();
        uiRestart.gameObject.SetActive(true);
        scoreManager.ScoreScreen(true);
        OnRestarted?.Invoke();
        
    }

    private void DestroyRemainingShips()
    {
        var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in remainingEnemies)
        {
            Destroy(enemy);
        }
        
        DeSpawnHpBarAndPlayer();

        var remainingEnemyBullets = GameObject.FindGameObjectsWithTag("EnemyBullet");
        foreach (var bullet in remainingEnemyBullets)
        {
            Destroy(bullet);
        }
        var remainingplayerBullets = GameObject.FindGameObjectsWithTag("PlayerBullet");
        foreach (var playerBullet in remainingplayerBullets)
        {
            Destroy(playerBullet);
        }
        var remaininghealer = GameObject.FindGameObjectsWithTag("Healer");
        foreach (var healer in remaininghealer)
        {
            Destroy(healer);
        }

        var remainingShootingStar = GameObject.FindGameObjectsWithTag("ShootingStar");
        foreach (var shootingstar in remainingShootingStar)
        {
            Destroy(shootingstar);
        }


    }
    private void OnRestartButtonOnclicked()
    {
       
        uiRestart.gameObject.SetActive(false);
        StartGame();
    }

    private void OnNextLevelButtonOnclicked()
    {
        SpawnHpBarAndPlayer();
        nextLevel.gameObject.SetActive(false);
        SpawnEnemySpaceshipLevel2();
    }

    private void OnBackToMenuButtonOnClicked()
    {
        scoreManager.SetScore(0);
        scoreManager.ScoreScreen(false);
        DeSpawnHpBarAndPlayer();
        DestroyRemainingShips();
        uiRestart.gameObject.SetActive(false);
        nextLevel.gameObject.SetActive(false);
        dialog.gameObject.SetActive(true);
    }
    
}
