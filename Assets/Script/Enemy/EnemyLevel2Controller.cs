﻿using Script.SpaceShip;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Enemy
{
    public class EnemyLevel2Controller : MonoBehaviour
    {
        
        [SerializeField] private EnemySpaceShipLevel2 enemySpaceShiplevel2;
        [SerializeField] private Rigidbody2D rigidbody2D;
         private float moveTime = 2 ;
         private float moveleft = 4.5f;

        private void Update()
        {
            moveTime += Time.deltaTime;
             Move();
            enemySpaceShiplevel2.Fire();
        }

        private void Move()
        {
            
            if (moveTime < moveleft)
            {
                MoveSet(Vector2.left);
            }
            if (moveTime > moveleft)
            {
                MoveSet(Vector2.right);
            }
            if (moveTime >= 9)
            {
                moveTime = 0;
            }
        }

        private void MoveSet(Vector2 direction)
        {
            rigidbody2D.velocity = direction * enemySpaceShiplevel2.Speed;
        }

    }    
}