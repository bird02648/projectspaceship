﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable
{
    event Action OnExploded;
    void TakeHit(int damage);
    void Explode();
}