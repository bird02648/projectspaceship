﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;


public class ShootingStartScript : MonoBehaviour
{
   [SerializeField] private Bullet shootingstar1;
   [SerializeField] private Transform star1;
   [SerializeField] private Bullet shootingstar2;
   [SerializeField] private Transform star2;
   [SerializeField] private Bullet shootingstar3;
   [SerializeField] private Transform star3;
   [SerializeField] private Bullet shootingstar4;
   [SerializeField] private Transform star4;
   static Random random = new Random();
   private float shootcount = 0;
   private float startShoot = 2;
   private void Update()
   {
      Fire();
   }

   public  void Fire()
   {
      shootcount += Time.deltaTime;
      if (shootcount > startShoot)
      {
         var numberRandom = random.Next(1, 5);
         if (numberRandom ==1)
         {
            var bullet = Instantiate(shootingstar1, star1.position, Quaternion.identity);
            bullet.Init(Vector2.down);
         }

         if (numberRandom ==2)
         {
            var bullet = Instantiate(shootingstar2, star2.position, Quaternion.identity);
            bullet.Init(Vector2.down);
         }

         if (numberRandom == 3)
         {
            var bullet = Instantiate(shootingstar3, star3.position, Quaternion.identity);
            bullet.Init(Vector2.down);
         }

         if (numberRandom ==4)
         {
            var bullet = Instantiate(shootingstar4, star4.position, Quaternion.identity);
            bullet.Init(Vector2.down);
         }

         shootcount = 0;
      }
   }

   
}
