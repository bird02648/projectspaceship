﻿using System;
using UnityEngine;

namespace Script.SpaceShip
{
    public class EnemySpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip enemyshipFireSounds ;
        [SerializeField] private float enemyshipFireSoundsValume = 0.1f;
        [SerializeField] private AudioClip enemyshipExplodedSounds;
        [SerializeField] private float enemyshipExplodedSoundsValume = 0.3f;
        [SerializeField] private double fireRate = 1;
        private float fireCounter = 0;
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyshipExplodedSounds,Camera.main.transform.position,enemyshipExplodedSoundsValume);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            // TODO : Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter > fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyshipFireSounds,Camera.main.transform.position,enemyshipFireSoundsValume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;

            }
           
        }
    }
}